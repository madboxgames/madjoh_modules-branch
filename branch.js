define([
	'require',
	'madjoh_modules/custom_events/custom_events',
	'madjoh_modules/session/session'
],
function(require, CustomEvents, Session){
	var BranchModule = {
		onDeviceReady : function(){
			Branch.initSession();
			CustomEvents.addCustomEventListener(document, 'LoggedIn', 	function(){BranchModule.login();});
			CustomEvents.addCustomEventListener(document, 'LoggingOut', function(){BranchModule.logout();});
		},
		onResume : function(){
			Branch.initSession();
		},
		init : function(settings){
			document.addEventListener('resume', 		BranchModule.onResume, 		false);
			document.addEventListener('deviceready', 	BranchModule.onDeviceReady, false);
		},
		login : function(){
			Branch.setIdentity(Session.getKeys().id)
			.then(function(res){console.log(res);})
			.catch(function(err){console.error(err);});
		},
		logout : function(){
			Branch.logout();
		}
	};

	return BranchModule;
});